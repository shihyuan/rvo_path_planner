#ifndef _RVO_PATHPLANNER_H
#define _RVO_PATHPLANNER_H

#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>

// #define _OPENMP

#ifdef _OPENMP
#include <omp.h>
#endif

#include "RVO.h"
#include "AgentTraj.h"

#ifndef M_PI
const float M_PI = 3.14159265358979323846f;
#endif

namespace RVO{
class RVOPathPlanner{ 
public:
  RVOPathPlanner();
  ~RVOPathPlanner();

  RVO::RVOSimulator *sim;
  std::vector<AgentTraj> agentTrajVec;
  std::vector<AgentTraj> refTrajVec; //Use as reference trajectory
  std::vector<Vector2> initPos;
  std::vector<Vector2> initVel;
  std::vector<float> maxSpeeds;
  std::vector<Vector2> goals;

  std::vector< std::vector<Vector2> > obstacles;
  bool hasRefTraj;


  // Default agent parameters 
  float neighborDist;
  int maxNeighbors;
  float timeHorizon;
  float timeHorizonObst;
  float radius;
  float maxSpeed;

  void setAgentParameters(float neighborDist, int maxNeighbors, float timeHorizon,
    float timeHorizonObst, float radius, float maxSpeed);

  // Simulation parameters
  float goalThreshold;
  float simTimeStep;
  int maxSteps;
  float minRadius;

  void setSimParameters(float goalThreshold, float simTimeStep, int maxSteps, float minRadius);
  int addAgent(Vector2 agentPos, Vector2 agentGoalPos, Vector2 agentVel, float maxSpeed);
  int addAgent(Vector2 agentPos, Vector2 agentGoalPos, Vector2 agentVel);
  int addAgent(Vector2 agentPos, Vector2 agentGoalPos);
  int addAgent(Vector2 agentPos);

  int addObstacle(std::vector<Vector2> obsVec);
  void setRefTraj();
  bool genPath(int numIterate);
  bool genPath();
  void resetSim();

  // void resetAgentPosition();
  Vector2 getPrefVelByGoal(Vector2 currentPos, Vector2 goalPos);
  Vector2 getPrefVelByRef(float currentTime, Vector2 currentPos, Vector2 goalPos, AgentTraj& agentTraj);

private:
  bool reachedGoal();
  void setPreferredVelocities();
  void logTrajData();
  void simStep();

  // Vector2 getPrefVelByRef();

};
}

#endif /* _RVO_PATHPLANNER_H */