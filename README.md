##Dependencies##
TODO:Add dependencies

##How to use rvo_path_planner to fly quads with rviz##

* Launch `raven_rviz` and `rvo_path_planner` by:
```
$ roslaunch rvo_path_planner rvo_raven_rviz.launch 
$ roslaunch rvo_path_planner rvo_path_planner.launch
```
* Launch the `quad_control`s (`sim:=1` for simulation)
```
$ roslaunch quad_control control.launch veh:=BQ num:=01 sim:=1
```
* In rviz, right-click the quad and select `Takeoff`. This will turn on the motors (this is required in sim too), but the quad won't/shouldn't generate enough lift to leave the ground.
* Activate the rvo_path_planner. The path planner will start sending commands to the quads through the `BQ0#/goal` topics:
```
$ rosservice call /rvo_path_planner/activate
```
* Use the up arrow of the interactive marker in rviz to lift the quad off the ground. Note that the command is sent when the interactive marker is released.
* Drag the box part of the interactive marker to send waypoints command. The path planner will plan a path when you release the marker and the quad will start following the path.
* To land the quad, use the down arrow of the interactive marker. It is recommended that you lose altitude gradually in several steps instead of all at once. When the quad is on the ground, right click the marker and select `Landed` will kill the motors (might not work in sim).
* You can also call the `/rvo_path_planner/land_all` service to land all the quads
* You can use the `/rvo_path_planner/deactivate` service call to stop the rvo_path_planner from publishing to the `BQ0#/goal` topics without killing the planner.