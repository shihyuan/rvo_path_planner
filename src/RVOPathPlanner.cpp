#include "RVOPathPlanner.h"

namespace RVO{

  RVOPathPlanner::RVOPathPlanner()
  {
    // Initialize simulator
    sim = NULL;
    // sim = new RVOSimulator();

    hasRefTraj = false;

    // Setup default paratmers
    neighborDist = 15.0f;
    maxNeighbors = 10;
    timeHorizon = 1.0f;
    timeHorizonObst = 1.0f;
    radius = 0.25f;
    maxSpeed = 2.5f;
    simTimeStep = 0.05f;
    goalThreshold = 0.05f;
    maxSteps = 200;
    minRadius = 0.1f;
    
  }
  RVOPathPlanner::~RVOPathPlanner()
  {
    // TODO: implement proper cleanup
    delete sim;
  }
  
  int RVOPathPlanner::addAgent(Vector2 agentPos, Vector2 agentGoalPos, Vector2 agentVel, float veh_maxSpeed)
  {
    initPos.push_back(agentPos);
    initVel.push_back(agentVel);
    goals.push_back(agentGoalPos);
    maxSpeeds.push_back(veh_maxSpeed);
    return goals.size()-1;    
  }
  
  int RVOPathPlanner::addAgent(Vector2 agentPos, Vector2 agentGoalPos, Vector2 agentVel)
  {
    return addAgent(agentPos,agentGoalPos,agentVel,maxSpeed) ;
  }

  int RVOPathPlanner::addAgent(Vector2 agentPos, Vector2 agentGoalPos)
  {
    return addAgent(agentPos, agentGoalPos, Vector2(0.0,0.0));
  }

  int RVOPathPlanner::addAgent(Vector2 agentPos)
  {
    return addAgent(agentPos, agentPos);
  }

  int RVOPathPlanner::addObstacle(std::vector<Vector2> obsVec)
  {
    obstacles.push_back(obsVec);
    return obstacles.size() - 1;
  }

  bool RVOPathPlanner::reachedGoal()
  {
    /* Check if all agents have reached their goals. */
    for (size_t i = 0; i < sim->getNumAgents(); ++i) {
      if (RVO::absSq(sim->getAgentPosition(i) - goals[i]) > goalThreshold*goalThreshold) {
        return false;
      }
    }
    return true;
  }

  void RVOPathPlanner::setPreferredVelocities()
  {
    /*
    * Set the preferred velocity to be a vector of unit magnitude (speed) in the
    * direction of the goal.
    */
    #ifdef _OPENMP
    #pragma omp parallel for
    #endif
    for (int i = 0; i < static_cast<int>(sim->getNumAgents()); ++i) {

      Vector2 prefVel;
      if (hasRefTraj){
        prefVel = getPrefVelByRef(sim->getGlobalTime(),
          sim->getAgentPosition(i),goals[i],refTrajVec[i]);
      }
      else{
        prefVel = getPrefVelByGoal(sim->getAgentPosition(i),goals[i]);
      }

      // sim->setAgentPrefVelocity(i, 5.0*prefVel);
      sim->setAgentPrefVelocity(i, sim->getAgentMaxSpeed(i)*prefVel);

      // Perturb a little to avoid deadlocks due to perfect symmetry.
      float angle = std::rand() * 2.0f * M_PI / RAND_MAX;
      float dist = std::rand() * 0.0001f / RAND_MAX;
      sim->setAgentPrefVelocity(i, sim->getAgentPrefVelocity(i) +
                                dist * Vector2(std::cos(angle), std::sin(angle)));
    }

  }

  void RVOPathPlanner::logTrajData()
  {
    for (int i = 0; i < sim->getNumAgents(); i++){
      agentTrajVec[i].log_data(sim->getGlobalTime(),
        sim->getAgentPosition(i),
        sim->getAgentVelocity(i),
        sim->getAgentPrefVelocity(i));
    }
  }

  void RVOPathPlanner::simStep()
  {
    setPreferredVelocities();
    logTrajData();
    if (sim->getGlobalTime() > 0.25){
      sim->setTimeStep(0.1);
    }
    sim->doStep();
  }

  bool RVOPathPlanner::genPath(){
    return genPath(0);
  }

  bool RVOPathPlanner::genPath(int numIterate)
  {
    bool goalReached;
    float radius_step;
    if (numIterate == 0){
      radius_step = 0;
    }
    else{
      radius_step = (radius - minRadius)/((float) numIterate);
    }

    for (int i = 0; i < numIterate + 1; i++){
      resetSim();
      goalReached = false;
      for (int step = 0; step < maxSteps; step++){
        simStep();
        if (reachedGoal()){
          goalReached = true;
          break;
        }
      }
      setRefTraj();
      // std::cout << ".";
      radius = minRadius + (float)i * radius_step;
    }
    // std::cout << std::endl;
    return goalReached;
  }

  void RVOPathPlanner::setAgentParameters(float neighborDist, int maxNeighbors, float timeHorizon, float timeHorizonObst, float radius, float maxSpeed)
  {
    this->neighborDist = neighborDist;
    this->maxNeighbors = maxNeighbors;
    this->timeHorizon = timeHorizon;
    this->timeHorizonObst = timeHorizonObst;
    this->radius = radius;
    this->maxSpeed = maxSpeed;
  }

  void RVOPathPlanner::setSimParameters(float goalThreshold, float simTimeStep, int maxSteps, float minRadius)
  {
    this->goalThreshold = goalThreshold;
    this->simTimeStep = simTimeStep;
    this->maxSteps = maxSteps;
    this->minRadius = minRadius;
  }

  void RVOPathPlanner::setRefTraj()
  {
    refTrajVec.clear();
    for (int i = 0; i < agentTrajVec.size();i++){
      refTrajVec.push_back(agentTrajVec[i]);
    }
    hasRefTraj = true;
  }

  void RVOPathPlanner::resetSim()
  {
    // (Re)Initialize the simulator

    if (sim != NULL){
      delete sim;
    }
    sim = new RVOSimulator();
    
    // Setup parameters
    sim->setAgentDefaults(neighborDist,maxNeighbors,timeHorizon,timeHorizonObst,radius,maxSpeed);
    // sim->setTimeStep(goalThreshold);
    sim->setTimeStep(simTimeStep);
    // Clear agentTrajVec
    agentTrajVec.clear();

    // Add agents to sim
    for (int i = 0; i < initPos.size(); i++){
      sim->addAgent(initPos[i]);
      sim->setAgentVelocity(i,initVel[i]);
      sim->setAgentMaxSpeed(i,maxSpeeds[i]);
      AgentTraj agentTraj;
      agentTraj.set_agent_id(i);
      agentTrajVec.push_back(agentTraj);
    }
    // Add obstacles
    for (int i = 0; i < obstacles.size();i++){
      sim->addObstacle(obstacles[i]);
    }
    sim->processObstacles();
  }

  Vector2 RVOPathPlanner::getPrefVelByGoal(Vector2 currentPos, Vector2 goalPos)
  {
    // Out of ref trajectory, just use the goal
    Vector2 goalVector = goalPos - currentPos;
    if (absSq(goalVector) > 1.0f) {
      goalVector = normalize(goalVector);
    }
    return goalVector;
  }

  Vector2 RVOPathPlanner::getPrefVelByRef(float currentTime, Vector2 currentPos, Vector2 goalPos, AgentTraj& agentTraj)
  {
    // Get index in trajectory according to current time
    std::vector<float>::iterator low;
    low = std::lower_bound (agentTraj.time_traj.begin(), agentTraj.time_traj.end(), currentTime);
    Vector2 goalVector;
    if (low == agentTraj.time_traj.end()){
      return getPrefVelByGoal(currentPos,goalPos);
    }
    else{
      int index = low - agentTraj.time_traj.begin();
      std::vector<RVO::Vector2>::iterator pos_iter;
      for(pos_iter = agentTraj.pos_traj.begin() + index; pos_iter != agentTraj.pos_traj.end(); pos_iter++){
        if (abs(currentPos - *pos_iter) > 0.25){
          return getPrefVelByGoal(currentPos,*pos_iter);
        }
      }
      return getPrefVelByGoal(currentPos,goalPos);
    }
  }

}