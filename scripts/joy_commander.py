#!/usr/bin/env python
# license removed for brevity
import rospy

# Include the ROS msg to be send
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseStamped
from rvo_path_planner.msg import CommandSequence
from sensor_msgs.msg import Joy


import numpy as np

class JoyCommander(object):
    def __init__(self):
        self.veh_name = rospy.get_param('~veh_name')
        self.gain = 1.0

        self.pub_command = rospy.Publisher('rvo_path_planner/command',CommandSequence, queue_size=1)
        self.sub_joy = rospy.Subscriber("/joy", Joy, self.callback_joy, queue_size=1)
        self.sub_veh_pose = rospy.Subscriber("/%s/pose" %self.veh_name,PoseStamped, self.callback_pose, queue_size=1)

        self.position = Point()
        self.joy_point = Point()

        self.landing = True

    def publish_waypoint(self,waypoint,waypoint_type=CommandSequence.MOVE):
        commandSeq = CommandSequence()
        commandSeq.veh_name = self.veh_name
        commandSeq.command = waypoint_type
        commandSeq.header.stamp = rospy.Time.now()
        commandSeq.waypoints.append(waypoint)
        self.pub_command.publish(commandSeq)

    # def update_waypoint_by_pos(self):
    #     self.waypoint.x = self.position.x
    #     self.waypoint.y = self.position.y
    #     self.waypoint.z = self.position.z

    def callback_pose(self,pose_msg):
        self.position = pose_msg.pose.position
        if not (self.joy_point.x == 0.0 and self.joy_point.y == 0.0):
            waypoint = Point()
            waypoint.x = self.position.x + self.joy_point.x
            waypoint.y = self.position.y + self.joy_point.y
            # waypoint.z = self.position.z + self.joy_point.z
            waypoint.z = 1.0
            if not self.landing:
                self.publish_waypoint(waypoint)
            # print waypoint

    def callback_joy(self,joy_msg):
        self.joy_point.x = self.gain*joy_msg.axes[4]
        self.joy_point.y = self.gain*joy_msg.axes[3]
        self.joy_point.z = 0.25*joy_msg.axes[1] #Currenlty not used.
        # print self.joy_point

        if (self.joy_point.x == 0.0 and self.joy_point.y == 0.0):

            if joy_msg.buttons[6] == 1:
                # Emergency Kill Switch: The back button
                waypoint = Point()
                waypoint.x = self.position.x
                waypoint.y = self.position.y
                waypoint.z = 0.0
                self.publish_waypoint(waypoint,CommandSequence.KILL)
                return 

            if joy_msg.buttons[0] == 1:
                self.landing = False
                self.publish_waypoint(self.position,CommandSequence.TAKEOFF)
            elif joy_msg.buttons[1] == 1:
                self.landing = True
                self.publish_waypoint(self.position,CommandSequence.LAND)
            else:
                if not self.landing:
                    waypoint = Point()
                    waypoint.x = self.position.x
                    waypoint.y = self.position.y
                    waypoint.z = 1.0
                    self.publish_waypoint(waypoint,CommandSequence.MOVE)


if __name__ == '__main__':
    rospy.init_node('joy_commander',anonymous=False)
    joyCommander = JoyCommander()
    rospy.spin()